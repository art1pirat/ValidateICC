#!/usr/bin/env perl 
# extracts all icc profiles from a given pdf into subdir
# Author: Andreas Romeyke
#
# example:
#   perl extract_all_iccs_from_pdf.pl test.pdf
#
#   results in creating a new subdir test.pdf_icc-profiles/
#
# needs the pdf-parser.py in PATH
# (https://didierstevens.com/files/software/pdf-parser_V0_4_3.zip)
#
use strict;
use warnings;
use utf8;
use Cwd 'getcwd';
use File::Path;

if ($#ARGV != 0) { die "$0 needs only one argument, got $#ARGV\n"; }
my $pdf = shift @ARGV;
# use it for a input relative output
#my $outdir = "${pdf}_icc_profiles/";

#use it for a caller relative output
my $currentpath=getcwd();
my $pdfbase = $pdf; $pdfbase=~s#[./]#_#g;
my $outdir = "$currentpath/${pdfbase}_icc_profiles/";

#print "outdir=$outdir\n";
mkpath $outdir, {verbose=>1};

## find all ICCbased objects
open (my $CMD, "python pdf-parser.py -s ICCBased $pdf |");
my @searchresults = 
# map {print "DEBUG1: '$_'\n"; $_}
map {$_=~s/\s+Referencing: (\d+) \d R/$1/g; $_}
map { chomp $_; $_}
grep {m/Referencing:/}
<$CMD>;
close $CMD;

foreach my $sr (@searchresults) {
    open (my $CMD2, "python pdf-parser.py -o $sr -f -d ${outdir}$sr.stream $pdf |");
    foreach my $line (<$CMD2>) {
        #    print "Debug2: $line"; 
    }
    close $CMD2;
}

1;
