#include <IccProfile.h>
#include <IccTag.h>
#include <IccIO.h>
#include <IccUtil.h>
#include <iostream>

/* derived from IccProfile.cpp, function "ValidateICCProfile" 
 * by Andreas Romeyke
 * please check License file "COPYING" for rights
 */

/* to extract ICC Profiles from TIFF images, use exiftool like:
 * exiftool -icc_profile -b -w icc image.tiff */

int main (int argc, char ** argv) {
  if (argc != 2) {
    std::cerr << "an ICC-filename expected, got '" << argc-1 << "' arguments" << std::endl;
      exit(EXIT_FAILURE);
  }
  char * filename = argv[1];
  auto pFileIO = new CIccFileIO;
  if (!pFileIO->Open(filename, "rb")) {
	std::cerr << "error reading ICC-file '" << filename << "', " << icValidateCriticalError << std::endl;
    delete pFileIO;
    exit(EXIT_FAILURE);
  }
  auto pIcc = new CIccProfile;
  // read and parse File
  std::string sReport;
  auto nStatusRV = pIcc->ReadValidate(pFileIO, sReport); /* ignore nStatusRV */
  delete pFileIO;
  // validate ICC profile
  auto nStatusV = pIcc->Validate(sReport);
  delete pIcc;
  std::cout << "ICC file '"<<filename << "' has Status "<< nStatusV << std::endl;
  std::cout << sReport << std::endl;
  return nStatusV;
}
